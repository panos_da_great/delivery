const express               = require('express');
const bodyParser            = require('body-parser');
const mongodbClient         = require("./controllers/mongo");
const configuration         = require('./config/configuration.json');
const callValidators        = require('./validations/callValidators');
const getCurrencyMultiplier = require('./validations/validateCurrency');

const dbOperations      = require('./data-control/dbOperations');

const app           = express();

const API_PORT      = process.env.PORT || 3000;

app.use(bodyParser.json({}));

/**
 * list all shops and basic props for homepage
 */
app.get("/shops", async (req, res) => {
    let status      = 200;
    let response    = { tstamp: Date.now(), data: [] };
    try {
        response.data = (await dbOperations.fetchShops()).shops; 
    } catch(e){
        console.error(e)
        console.error(`Expception > ${e}`)
        status = 500;
        response.error = e.message  || "INTERNAL_ERROR";
    } finally {
        res.status(status).json(response)
    }
});

/**
 * list menu from the selected shop
 * req querys { currency } || euro
 */
app.get("/shops/:shop_id", async (req, res) => {
    let status      = 200;
    let response    = { tstamp: Date.now(), data: { currency: req.query.currency || "EUR" } };
    try {
        const currencyMultiplier = await getCurrencyMultiplier(req.query);
        response.data            = { ...response.data, currencyMultiplier, ...await dbOperations.fetchMenu(req.params.shop_id, currencyMultiplier)}; 
        console.log("RES: ",response)
    } catch(e){
        console.error(e)
        console.error(`Expception > ${e}`)
        status = 500;
        response.error = e.message  || "INTERNAL_ERROR";
    } finally {
        res.status(status).json(response)
    }
});

/**
 * req.body: { cookie_session_id, items: [{ ...item, quantity } ], total_price, currency, currencyMultiplier }
 */
app.post("/shops/:shop_id/add-to-cart", async (req, res) => {
    let status      = 200;
    let response    = { tstamp: Date.now(), data: null };
    try {
        response.data = await dbOperations.addToCart({ _id: req.body.cookie_session_id }, { ...req.body, shop_id: req.params.shop_id, tstamp: response.tstamp });
    } catch(e){
        console.error(e)
        console.error(`Expception > ${e}`)
        status = 500;
        response.error = e.message  || "INTERNAL_ERROR";
    } finally {
        res.status(status).json(response)
    }
});

/**
 * req.body: { cookie_session_id, address, address_num,  doorbell, , floor, city, comments, mail }
 */
app.post("/shops/:shop_id/submit-order", async(req, res) => {
    let status      = 200;
    let response    = { tstamp: Date.now(), data: [] };
    try {
        response.data = await dbOperations.submitOrder({ _id: req.body.cookie_session_id }, req.body)
    } catch(e){
        console.error(e)
        console.error(`Expception > ${e}`)
        status = 500;
        response.error = e.message  || "INTERNAL_ERROR";
    } finally {
        res.status(status).json(response)
    }
});

app.get("/merchant/:_id/orders", async(req, res) => {
    let status      = 200;
    let response    = { tstamp: Date.now(), data: [] };
    try {
        response.data = await dbOperations.fetchOrders(req.params._id);
    } catch(e){
        console.error(e)
        console.error(`Expception > ${e}`)
        status = 500;
        response.error = e.message  || "INTERNAL_ERROR";
    } finally {
        res.status(status).json(response)
    }
})

/** 
 * req params { name category price }
 */
app.get("/merchant/:_id/add-item", async (req, res) => {
    let status = 200;
    let response    = { tstamp: Date.now(), status: null };
    try {
        console.log("validate item : ", JSON.stringify(req.query))
        callValidators(req.query, configuration.api.callValidations["ADD-ITEM"]);
        response.status = await dbOperations.addItem({...req.params, ...req.query});

    } catch (e) {
        console.error(e)
        console.error(`Expception > ${e}`)
        status          = e.statusCode || 500;
        response.error  = e.message || "INTERNAL_ERROR";
        response.status = e.status  || "FAILED"; 
    } finally {
        res.status(status).json(response)
    }
})

mongodbClient.eventEmitter.on("MongoClientConnected", async (db) => {
    app.listen(API_PORT, () => { console.log(`Delivery API listens at port ${API_PORT}`) });
});