const datafixerApi = require('../api/datafixer');

module.exports = async ({currency}) => {
    try {
        if (!currency || currency.toUpperCase() === "EUR") {
            return null;
        }

        const res = await datafixerApi.get("http://data.fixer.io/api/latest?access_key=7eadb73f3fa69c2f745d421b8ad14b75");

        if (!res.data.success){
            throw "CURRENCY_API_INTERNAL_ERROR";
        } 

        const rate = res.data.rates[currency.toUpperCase()];
        
        if (!rate) {
            throw "ΝΟΤ_SUPPORTED_CURRENCY";
        }

        return rate;
    } catch(e) {
        console.error("exchange Rates External API exception: ", e);
        throw e;
    }
}