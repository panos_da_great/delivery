
module.exports = (params, mandatoryFields) => {
    if (!params  || Object.keys(mandatoryFields).length !== Object.keys(params).length) {
        throw { message: "INVALID_CALL", status: "VALIDATION_FAILURE", statusCode: 400 };
    }

    for(key of Object.keys(mandatoryFields)) {
        
        if (!params[key])   {
            throw { message: "MISSING_PARAMETER", status: "VALIDATION_FAILURE", statusCode: 400 };
        } else if (typeof params[key] !== mandatoryFields[key]) {
            if (mandatoryFields[key] === "number" && !isNaN(Number(params[key]))) {
                continue;
            } 
            throw { message: "INVALID_PARAMETER", status: "VALIDATION_FAILURE", statusCode: 400 };
            
        }
    }
}