const { ObjectId }          = require('mongodb')
const events                = require('events');
const mongo                 = require("../controllers/mongo");
const net_payout_percentage = require('../config/configuration.json').net_payout_percentage;

const emitter                    = new events.EventEmitter();
const { successRes, errorRes }   = require("./dataResponses");

emitter.on("update-shops-revenues", async({shop_id, revenues}) => {
    console.log("Event update-shops-revenues", JSON.stringify({shop_id, revenues}));
    try {
        const query = { _id: ObjectId(shop_id) };
        mongo.Shops().findOne(query, { projection: { _id:0, revenues:1 } }, (err, doc) => {
            if (err) {
                throw err;
            }
            const net_payout =  revenues * (doc.revenues.net_payout_percentage || net_payout_percentage);

            mongo.Shops().updateOne(query, { 
                $inc: { "revenues.total": Number(revenues), "revenues.net_payout":  Number(net_payout) }
            });

        });
    } catch(e) {
        console.error("update-merchands-revenue > exception: ", e)
    }
})

module.exports = {
    fetchShops: async() => {
        try {
            const shops = await mongo.Shops().find({ active: true }, { projection: { name: 1, type: 1, delivery: 1, rate: 1, tags:1 } }).sort({ type: 1 }).toArray();
            return successRes({ shops });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    },

    fetchMenu: async (shop_id, currencyMultiplier) => {
        try {
            shop_id = ObjectId(shop_id);
            const items = await mongo.Items().find({ shop_id }).sort({ category: 1 }).toArray();
            
            if (items.length === 0) {
                throw "EMPTY_MENU";
            }

            let categories = {};

            for (var i = 0; i < items.length; i++) {
                if (currencyMultiplier) {
                    items[i].price *= currencyMultiplier;
                    items[i].price = items[i].price.toFixed(2)
                }

                if (!categories[items[i].category]) {
                    categories[items[i].category] = [];
                }
                categories[items[i].category].push(items[i]);
            }
            const menu = { items, categories, itemCategories: Object.keys(categories)};
            
            return successRes({ menu })
        } catch (e) {
            throw errorRes("MONGO_FAILED", e);
        }
    },

    addItem: async({ _id, name, category, price, }) => {
        try {
            const { result } = await mongo.Items().insertOne({
                name, category, price,
                "enabled":true,
                "available":true,
                "created_at": Date.valueOf(),
                "updated_at": null,
                "shop_id": ObjectId(_id)
            });
            return successRes({ result });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    },

    addToCart: async(query, update) => {
        try {
            update._id = update.cookie_session_id;
            update.shop_id = ObjectId(update.shop_id);
            delete update.cookie_session_id;
            await mongo.Carts().updateOne(query, { $set: update }, { upsert: true });
            return successRes({ cart: update });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    },

    submitOrder: async(query, order) => {
        try {
            const cart = await mongo.Carts().findOneAndDelete(query);
            
            if (!cart.value) {
                throw "FAILED_TO_RETREIVE_CART"
            }

            const revenues = cart.value.currency === "EUR" ? cart.value.total_price : (cart.value.total_price / cart.value.currencyMultiplier).toFixed(2);
            delete order.cookie_session_id;
            await mongo.Orders().insertOne({...order, ...cart.value, status: "new", revenues, order_tstamp: new Date()});
            emitter.emit("update-shops-revenues", { shop_id: cart.value.shop_id, revenues });
            return successRes({ order: {...order, ...cart.value} });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    },

    fetchOrders: async shop_id => {
        try {
            shop_id = ObjectId(shop_id)
            const orders = await mongo.Orders().find({ shop_id, status: "new" }).sort({ order_tstamp: 1 }).toArray();
            return successRes({ orders });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    },

    updateShopsRevenues: (query, update) => {
        mongo.Shops().upadteOne(query, update);
    },
} 