# README #

HOW TO DEPLOY delivery-API

### Clone Repository ###

* git clone https://panos_da_great@bitbucket.org/panos_da_great/delivery.git 

### Install Dependencies ###

* cd delivery
* npm install

### start service ###

* pm2 start ecosystem.config.js --env production

### Can I run a test scenario? ###

* You can run two order test scenarios
* cd tests
* jest apiFlow.test.js --detectOpenHandles