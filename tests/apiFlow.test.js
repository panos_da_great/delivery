process.env.NODE_ENV = "production"
const api   = require('./utils/deliveryApi');
const mongo = require("../controllers/mongo");
const { ObjectId } = require('mongodb');

beforeAll(async() => {
    await mongo.awaitConnection();
})
describe("API sequential Test", () => {

    test("Make an order flow in USD", async() => {
        const scenarioSelection = {
            shop: 'CoffeeShop',
            items: [ 
                { name: "Freddo Espresso", quantity: 2, comments: "1 with Brown Sugar and one black" },
                { name: "Orange Juice", quantity: 1,  comments: "" },
            ]
        }

        const res = await api.get("/shops");
        const selectshop = res.data.data.find(s => s.name === scenarioSelection.shop);
        const listShopsItems = await api.get("/shops/" + selectshop._id + "?currency=USD");

        const cookie_session_id = Date.now();
        const shopMenu = listShopsItems.data.data.menu.items;
        
        let _items = [];
        let total_price = 0;

        for(let item of scenarioSelection.items) {

            const selectItem = { ...shopMenu.find(i => item.name === i.name), ...item };
            total_price = parseFloat(selectItem.price) * selectItem.quantity;
            _items.push(selectItem);

            const { cart } = ( await api.post(`/shops/${selectshop._id}/add-to-cart`, {
                cookie_session_id,
                items: _items,
                total_price,
                currency:               listShopsItems.data.data.currency,
                "currencyMultiplier":   listShopsItems.data.data.currencyMultiplier
            }) ).data.data;
        }

        const myCart = await mongo.Carts().findOne({ _id: cookie_session_id });

        //Cart should be:
        expect(myCart.items).toEqual(_items);
        expect(myCart.total_price).toBe(total_price);

        const order = ( await api.post(`/shops/${selectshop._id}/submit-order`, {
            cookie_session_id,
            "address": "Artemidos",
            "address_num": "14",
            "city": "Paleo Faliro",
            "doorbell": "Smith",
            "comments": "",
            "mail": "pa@yahoo.gr"
        })).data.data.order
        

        const myOrder = await mongo.Orders().findOne({ _id: cookie_session_id });

        //Order should be:
        expect(myOrder.items).toEqual(order.items);
        expect(myOrder.items).toEqual(_items);
        expect(myOrder.total_price).toBe(total_price);

    });

    test("Make an order flow in EUR", async() => {
        const scenarioSelection = {
            shop: 'CoffeeShop',
            items: [ 
                { name: "Freddo Espresso", quantity: 2, comments: "1 with Brown Sugar and one black" },
                { name: "Orange Juice", quantity: 1,  comments: "" },
            ]
        }

        const res = await api.get("/shops");
        const selectshop = res.data.data.find(s => s.name === scenarioSelection.shop);
        const listShopsItems = await api.get("/shops/" + selectshop._id);

        const cookie_session_id = Date.now() + 1000;
        const shopMenu = listShopsItems.data.data.menu.items;
        
        let _items = [];
        let total_price = 0;

        for(let item of scenarioSelection.items) {

            const selectItem = { ...shopMenu.find(i => item.name === i.name), ...item };
            total_price = parseFloat(selectItem.price) * selectItem.quantity;
            _items.push(selectItem);

            const { cart } = ( await api.post(`/shops/${selectshop._id}/add-to-cart`, {
                cookie_session_id,
                items: _items,
                total_price,
                currency:               listShopsItems.data.data.currency,
                "currencyMultiplier":   listShopsItems.data.data.currencyMultiplier
            }) ).data.data;
        }

        const myCart = await mongo.Carts().findOne({ _id: cookie_session_id });

        //Cart should be:
        expect(myCart.items).toEqual(_items);
        expect(myCart.total_price).toBe(total_price);

        const order = ( await api.post(`/shops/${selectshop._id}/submit-order`, {
            cookie_session_id,
            "address": "Artemidos",
            "address_num": "14",
            "city": "Paleo Faliro",
            "doorbell": "Smith",
            "comments": "",
            "mail": "pa@yahoo.gr"
        })).data.data.order
        

        const myOrder = await mongo.Orders().findOne({ _id: cookie_session_id });

        //Order should be:
        expect(myOrder.items).toEqual(order.items);
        expect(myOrder.items).toEqual(_items);
        expect(myOrder.total_price).toBe(total_price);

    });
})