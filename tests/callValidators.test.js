const callValidators = require('../validations/callValidators')

describe("TEST callValidators.js", () => {
    test("Proper params", () => {
        const params = { _id: "5fa6b3ad0a47bf1a7c2b48ff",  name: "Pork Steak", category: "main_dishes",  price: 6.4 }
        const mandatoryFields =  { _id: "string", "name" : "string", "category": "string", "price": "number" };
        const c = callValidators(params, mandatoryFields)
        expect(c).toBe(undefined);
    });

    test("Missing params", () => {
        const params = { _id: "5fa6b3ad0a47bf1a7c2b48ff",  name: "Pork Steak", category: "main_dishes",  }
        const mandatoryFields =  { _id: "string", "name" : "string", "category": "string", "price": "number" };
        try {
            const c = callValidators(params, mandatoryFields);
        } catch(e) {
            expect(e).toEqual({"message": "INVALID_CALL", "status": "VALIDATION_FAILURE", "statusCode": 400});
        }
        
    })

    test("Numerical param", () => {
        const params = { _id: "5fa6b3ad0a47bf1a7c2b48ff",  name: "Pork Steak", category: "main_dishes", price: "6.5" }
        const mandatoryFields =  { _id: "string", "name" : "string", "category": "string", "price": "number" };
        const c = callValidators(params, mandatoryFields);
            expect(c).toBe(undefined);
        
    })

    test("mISSING param", () => {
        const params = { _id: "5fa6b3ad0a47bf1a7c2b48ff",  name: "Pork Steak", category: "main_dishes", price: "6.5" }
        const mandatoryFields =  { _id: "string", "name" : "string", "_category": "string", "price": "number" };
        try {
            const c = callValidators(params, mandatoryFields);
        } catch(e) {
            expect(e).toEqual({"message": "MISSING_PARAMETER", "status": "VALIDATION_FAILURE", "statusCode": 400});
        }
        
    })

    test("mISSING param", () => {
        const params = { _id: "5fa6b3ad0a47bf1a7c2b48ff",  name: "Pork Steak", type: "main_dishes", price: "6.5" }
        const mandatoryFields =  { _id: "string", "name" : "string", "category": "string", "price": "number" };
        try {
            const c = callValidators(params, mandatoryFields);
        } catch(e) {
            expect(e).toEqual({"message": "MISSING_PARAMETER", "status": "VALIDATION_FAILURE", "statusCode": 400});
        }
        
    })
})