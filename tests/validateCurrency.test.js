const exchangeRates = require('../validations/validateCurrency');

describe("TEST exchangeRates.js", () => {
    test ("Sending EUR || nothing", async() => {
        const query = {}
        const res = await exchangeRates(query);
        expect(res).toBe(null);
    })

    test ("Sending AED", async() => {
        const query = {currency: "AED"}
        const res = await exchangeRates(query);
        expect(res).toBeGreaterThan(4);
    })

    test ("Sending DRH", async() => {
        try {
            const query = {currency: "DRH"}
            const res = await exchangeRates(query);
        } catch (e) {
            expect(e).toBe("ΝΟΤ_SUPPORTED_CURRENCY");
        } 
    })
})